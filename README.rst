========
Overview
========

Build Badges:

Master: |master|
Develop: |develop|

.. |master| image:: https://gitlab.com/aaroncwhite/personal-util-kit/badges/master/pipeline.svg
    :target: https://gitlab.com/aaroncwhite/personal-util-kit/commits/master
    :alt: Master Branch

.. |develop| image:: https://gitlab.com/aaroncwhite/personal-util-kit/badges/develop/pipeline.svg
    :target: https://gitlab.com/aaroncwhite/personal-util-kit/commits/develop
    :alt: Develop Branch

A set of utilities that I have collected over time and am tired of maintaining in multiple places. Having a
library configured allows me to re-use code across multiple projects. I don't expect it to be very useful
beyond my projects, but is a code sample nonetheless. 

Note: this repository uses Semantic Versioning and follows Git Flow workflow. 
SemVer: semver.org
GitFlow: https://datasift.github.io/gitflow/IntroducingGitFlow.html

* Free software: MIT license

Installation
============

::

    pip install git+https://gitlab.com/aaroncwhite/personal-util-kit#egg=util-kit

Documentation
=============

Is sparse, but documentation can be found in line currently.  Using Sphinx to render documentation is on the TODO list.


Development
===========

To run the all tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
