from unittest import TestCase
import tempfile
from pathlib import Path
import json
from hyperopt.pyll.base import Apply as hyperopt_Apply

from ..hyperopt_config import *

CONFIG1 = {
    'batch_size': {
        'params': 128
    },
    'epochs': {
        'params': 5
    },
    # This is the only expected variable parameter
    'embedding_size': {
        'params': {
            'distribution': 'choice',
            'args': [[100, 200, 300, 500, 1000]],
            'name': 'embedding_size'},
        'hyperopt': True}
}

CONFIG2 = {
    'batch_size': {
        'params': 128
    },
    'epochs': {
        'params': 5
    },
    'embedding_size': {
        'params': {
            'distribution': 'choice',
            'args': [
                [100, 200, 300, 500, 1000]
            ],
            'name': 'embedding_size'
        },
        'hyperopt': True
    },
    'lstm_size': {
        'params': {
            'distribution': 'choice',
            'args': [
                [100, 200, 500, 50]
            ],
            'name': 'lstm_size'
        },
        'hyperopt': True
    },
    'conv_filters': {
        'params': {
            'distribution': 'choice',
            'args': [
                [100, 400, 300, 50]
            ],
            'name': 'conv_filters'
        },
        'hyperopt': True
    },
    'conv_activation': {
        'params': {
            'distribution': 'choice',
            'args': [
                ['sigmoid', 'relu', 'softmax']
            ],
            'name': 'conv_activation'
        },
        'hyperopt': True
    },
    'conv_padding': {
        'params': 'valid'
    },
    'conv_kernel': {
        'params': {
            'distribution': 'choice',
            'args': [
                [2, 3, 4, 5, 6, 7]
            ],
            'name': 'conv_kernel'
        },
        'hyperopt': True
    },
    'conv_strides': {
        'params': {
            'distribution': 'choice',
            'args': [
                [2, 10, 3, 4, 5]
            ],
            'name': 'conv_strides'
        },
        'hyperopt': True
    },
    'pool_size': {
        'params': {
            'distribution': 'choice',
            'args': [
                [2, 4, 6]
            ],
            'name': 'pool_size'
        },
        'hyperopt': True
    },
    'final_activation': {
        'params': 'sigmoid'
    },
    'loss': {
        'params': {
            'distribution': 'choice',
            'args': [
                ['weighted', 'categorical_hinge', 'categorical_crossentropy']
            ],
            'name': 'loss'
        },
        'hyperopt': True
    }
}

BAD_CONFIG = {
    'batch_size': {
        'params': 128
    },
    'epochs': {
        'params': 5
    },
    # This is the only expected variable parameter
    'embedding_size': {
        'params': {
            'distribution': 'chioce', # Sometimes we all type things wrong
            'args': [[100, 200, 300, 500, 1000]],
            'name': 'embedding_size'},
        'hyperopt': True}
}


class TestHyperoptConfig(TestCase):

    def setUp(self):
        # Only make a temp dir once
        self._td = tempfile.TemporaryDirectory()
        self.td = Path(self._td.name)

    def tearDown(self):
        self._td.cleanup()


    def test_from_dict(self):
        # This should pass
        hyper_config = HyperoptConfig(**CONFIG1)
        # Confirm expected
        self.assertEqual(hyper_config.batch_size, 128)
        self.assertEqual(hyper_config.epochs, 5)
        self.assertIsInstance(hyper_config.embedding_size, HyperoptParameter)

    def test_yaml_io(self):
        # HyperoptConfig should be able to write out to yaml and 
        # read back in, creating the same configuration
        hyper_config = HyperoptConfig(**CONFIG2)
        hyper_config.to_yaml(self.td / 'config2_test.yml')
        new_config = hyper_config.from_yaml(self.td / 'config2_test.yml')
        # use a sorted json str to create hashable data
        configs = [json.dumps(i.to_dict(), sort_keys=True) for i in [hyper_config, new_config]]
        
        self.assertEqual(configs[0], configs[1])

    def test_parameter_raise(self):
        self.assertRaises(TypeError, HyperoptConfig, BAD_CONFIG)

    def test_param_not_dict(self):
        self.assertRaises(HyperoptConfigError, HyperoptConfig, batch_size=128)

    def test_hyperopt_translation(self):
        config = HyperoptConfig(**CONFIG1)
        # The .get_space() translates into hyperopt properties
        config_space = config.get_space()
        self.assertIsInstance(config_space['embedding_size'], hyperopt_Apply)

