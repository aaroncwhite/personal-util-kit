from unittest import TestCase
from ..losses import *


class TestWeightedLoss(TestCase):
    # This will only test the supporting calculations
    # not the loss itself

    def test_weight_calc(self):
        # this test is somewhat redundant since it relies on scikit
        a = np.array([[1, 0], 
                      [1, 0], 
                      [0, 1]])

        weights = np.array([[1.5 , 0.75],
                            [0.75, 1.5 ]])
        
        w = WeightedLoss(a)

        self.assertTrue(np.array_equal(weights, w.weights))
