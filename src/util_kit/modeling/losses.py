import numpy as np
import tensorflow.keras.backend as K
from sklearn.utils.class_weight import compute_class_weight

# from https://stackoverflow.com/questions/48485870/multi-label-classification-with-class-weights-in-keras
# NOTE: K is keras.backend

class WeightedLoss(object):
    '''
    Created a weighted loss function for multiple output class predictions
    '''

    def __init__(self, y_true):
        self.weights = self.calculate_class_weights(y_true)

    def __call__(self, y_true, y_pred, sample_weight=None):
        """Calculate weighted loss on predicted values
        """
        return K.mean((self.weights[:, 0]**(1-y_true))*(self.weights[:, 1]**(y_true))*K.binary_crossentropy(y_true, y_pred), axis=-1)

    def calculate_class_weights(self, y_true):
        """Calculate weights for true values per column in the y_true data

        Arguments:
            y_true (array): an array of output data for the model
        """
        number_dim = np.shape(y_true)[1]
        weights = np.empty([number_dim, 2])
        for i in range(number_dim):
            weights[i] = compute_class_weight(
                'balanced', np.unique(y_true[:, i]), y_true[:, i])
        return weights
