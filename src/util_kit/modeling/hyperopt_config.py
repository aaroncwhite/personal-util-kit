import itertools
import yaml
import json
import pprint
from copy import deepcopy
from hyperopt import hp

class HyperoptConfigError(Exception):
    pass

class HyperoptConfig(object):
    """A container to store configuration details for use with
    Hyperopt. Because I didn't want to have to keep typing dictionaries out.

    Each keyword must be a dictionary with at least a key called 'params'.  
    It is a static parameter unless the dictionary has a child key named 
    'hyperopt' set to true.  The ultimate config should be used as normal 
    with the standard Hyperopt workflow.  This makes it easier to store
    the configuration either in a dictionary or as a yaml file.

    Ex.:
    >>> config = {
        'batch_size': {
            'params': 128
        },
        'epochs': {
            'params': 5
        },
        # This is the only expected variable parameter
        'embedding_size': {
            'params': {
                'distribution': 'choice',
                # Because args end up expanding when passed in
                # this needs to be nested one level
                'args': [[100, 200, 300, 500, 1000]],
                'name': 'embedding_size'},
            'hyperopt': True}
    }
    >>> hyperconfig = HyperoptConfig(**config)
    >>> def run_model(**kwargs):
            # run some model here, return the 
            # metric you want to optmize
            return 1
    >>> from hyperopt import tpe, fmin, Trials
    >>> results = fmin(
        run_model,
        algo=tpe.suggest,
        space=hyperconfig.get_space(),
        trials=Trials(),
        max_evals=5
    )

    More information on Hyperopt can be found here_.

    .. _here: https://hyperopt.github.io/hyperopt/
    
    """

    def __repr__(self):
        return pprint.pformat(self.params(), indent=2)
    
    def __init__(self, **params):
        
        for k,v in params.items():
            if not isinstance(v, dict):
                raise HyperoptConfigError('All parameters passed to __init__ must be dictionaries.')
            
            if v.get('hyperopt'):
                v = HyperoptParameter.from_dict(v['params'])
            
            else:
                v = v['params']
            
            setattr(self, k, v)
    
    def params(self):
        return {k : getattr(self, k) for k in dir(self) 
                if not k.startswith('_') and (not callable(getattr(self, k)) or isinstance(getattr(self, k), HyperoptParameter))}
    
  
    @classmethod
    def from_yaml(cls, filepath):
        """Imports a configuration defined in a YAML file
        
        Arguments:
            filepath {str or os.Path} -- Where the file is located

        Returns:
            HyperparameterConfig -- An instance of the Hyperparameter config
        """
        with open(filepath, 'r') as f:
            config = yaml.safe_load(f)

        return cls(**config)

    def to_yaml(self, filepath):
        """Saves a HyperparameterConfig to disk
        
        Arguments:
            filepath {str or os.Path} -- where the file should be saved
        """

        with open(filepath, 'w') as f:
            msg = ['# Hyperparameter Configuration file', 
                    '# Reload with HyperparameterConfig.from_yaml(this_file_path)\n\n']
            f.write('\n'.join(msg))
            tmp = json.dumps(self.to_dict(), sort_keys=True)
            tmp = json.loads(tmp)
            f.write(yaml.dump(tmp, default_flow_style=False))


    def to_dict(self):
        out = {}
        for k,v in self.params().items():
            if isinstance(v, HyperoptParameter):
                out[k] = {'hyperopt': True, 'params' : v.to_dict()}
            else:
                out[k] = {'params': v}
        return out
            
    def get_space(self):
        x = self.params()
        out = {}
        for k,v in x.items():
            out[k] = v if not isinstance(v, HyperoptParameter) else v()
            
        return out
    
    
class HyperoptParameter:
    """A descriptor used in conjunction with a 
    HyperoptConfig.  Stores parameter data to return
    a valid hyperopt symbolic parameter for use with
    TPE Optimization.

    Arguments:
        distribution (str): A valid distribution supported by Hyperopt

    Returns:
        hyperopt.pyll.base.Apply: A symbolic parameter for use with Hyperopt's modeling
    
    Ex.:
    class MyConfig:
        normal_param = 1
        hyperparam   = HyperoptParameter('choice', [1,2,3]) 

    """
    
    def __init__(self, distribution, *args, **kwargs):
        self.distribution = distribution
        self.args = args
        if kwargs.get('name'):
            self.name = kwargs.pop('name')
            
        self.kwargs = kwargs
        self._validate()
        
    def _validate(self):
        if self.distribution not in hp.__dict__.keys():
            raise TypeError("{} is not a known parameter estimator from hyperopt.hp".format(self.distribution))

    
    def __get__(self, config, cls):
        if config:
            self.name = self._name(config)
            return self
        else:
            return self.__class__
        
    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self.__dict__)
    
    def __call__(self):
        return hp.__dict__[self.distribution](self.name, *self.args, **self.kwargs)
    

    @classmethod
    def from_dict(cls, dct):
        dictionary = deepcopy(dct)
        distribution = dictionary.pop('distribution', None)
        args = dictionary.pop('args', ())
        name = dictionary.pop('name', None)
        kwargs = dictionary.pop('kwargs', {})
        return cls(distribution, name=name, *args, **kwargs)
    
    def to_dict(self):
        return {k:v for k,v in self.__dict__.items() if v}
    
    def _name(descriptor, instance):
        # The HyperoptParameter is a descriptor on the parent Config
        # class, so needs some help when getting it's name depending
        # on what the config state is at the time
        if not instance:
            return ''
        
        attributes = set()
        for cls in type(instance).__mro__:
            # add all attributes from the class into `attributes`
            # you can remove the if statement in the comprehension if you don't want to filter out attributes whose names start with '__'
            attributes |= {attr for attr in dir(cls) if not attr.startswith('__')}
        for attr in attributes:
            try:
                if type(instance).__dict__[attr] is descriptor:
                    
                    return attr
            except:
                pass
