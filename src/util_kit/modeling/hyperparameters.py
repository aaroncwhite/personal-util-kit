import itertools
import yaml
import json
import pprint
from collections import OrderedDict

class HyperparameterConfig(object):

    def __repr__(self):
        return pprint.pformat(self.params(), indent=2)
    
    def __init__(self, **params):
        
        for k,v in params.items():
            if v['variable']:
                v = VariableHyperparameter.from_dict(**v['params'])
            
            else:
                v = v['params']
            
            setattr(self, k, v)
    
    def params(self):
        return {k : getattr(self, k) for k in dir(self) 
                if not k.startswith('_') and not callable(getattr(self, k))}
    
    def _attr_to_list(self, x):
        try:
            return x.grid()
        except:
            return [x]
    
    @classmethod
    def from_yaml(cls, filepath):
        """Imports a configuration defined in a YAML file
        
        Arguments:
            filepath {str or os.Path} -- Where the file is located

        Returns:
            HyperparameterConfig -- An instance of the Hyperparameter config
        """
        with open(filepath, 'r') as f:
            config = yaml.safe_load(f)

        return cls(**config)

    def to_yaml(self, filepath):
        """Saves a HyperparameterConfig to disk
        
        Arguments:
            filepath {str or os.Path} -- where the file should be saved
        """

        with open(filepath, 'w') as f:
            msg = ['# Hyperparameter Configuration file', 
                    '# Reload with HyperparameterConfig.from_yaml(this_file_path)\n\n']
            f.write('\n'.join(msg))
            f.write(yaml.dump(self.to_dict(), default_flow_style=False))



    def to_dict(self):
        x = self.params()
        out = {}
        for k,v in x.items():
            out[k] = {'variable' : False, 'params' : v} \
                        if not isinstance(v, VariableHyperparameter)  \
                            else {'variable': True, 'params' : v.to_dict()}
            
        return out
    
    def grid(self):
        params = self.params()
        if params:
            keys, values = zip(*params.items())
            values = (self._attr_to_list(v) for v in values)
            experiments = [dict(zip(keys, v)) for v in itertools.product(*values)]
            return experiments
        else:
            return []
   
    
    
class VariableHyperparameter:
    
    def __init__(self,max_combinations=1, arg_options=None, add_none=True, strategy='combinations', **kwarg_options):
        self.max_combinations = max_combinations
        self.kwarg_options = kwarg_options
        self.arg_options = arg_options
        self.add_none = add_none
        self.strategy = strategy
    
    
    def __get__(self, config, config_class):
        if config:
            return self
        else:
            return self.__class__
        
    def __repr__(self):
        return 'VariableHyperparameter({})'.format(str(self.to_dict()))
    
    @classmethod
    def from_dict(cls, **kwargs):
        kwarg_options = kwargs.get('kwarg_options')
        if kwarg_options:
            kwargs.pop('kwarg_options')
        else:
            kwarg_options = {}

        return cls(**kwargs, **kwarg_options)
        

    def to_dict(self):
        return {k:v for k,v in self.__dict__.items() if not k.startswith('_') and v}
    
    def grid(self):
        if self.kwarg_options:
            keys, values = zip(*self.kwarg_options.items())
            experiments = [dict(zip(keys, v)) for v in itertools.product(*values)]
            experiments = all_combinations(experiments, self.max_combinations, self.strategy)

        else:
            if self.max_combinations == 1:
                experiments = self.arg_options
            else:
                experiments = combinations(self.arg_options, self.max_combinations)
            
        
        if self.add_none:
            experiments = itertools.chain(experiments, [None])
            
        return experiments
    

def all_combinations(any_list, n=None, strategy='combinations'):
    if not n:
        n = len(any_list)
        
    return itertools.chain.from_iterable(
        itertools.__dict__[strategy](any_list, i + 1)
        for i in range(n))
