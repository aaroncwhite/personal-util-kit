from unittest import TestCase
import tempfile
from pathlib import Path


class TempdirTestCase(TestCase):
    """Utility class that sets up a temporary 
    directory during testing and closes at
    the end of the tests.

    Default setUp and tearDown methods are baked
    by default, or use .make_tmp() and .destroy_tmp()
    for more control in custom setup and teardown.
    """

    def setUp(self):
        """Default setup.  Make a temp directory and 
        a path attribute for use in tests
        """
        self.make_tmp()


    def make_tmp(self):
        """Make a temp directory for use with tests
        """
        self._td = tempfile.TemporaryDirectory()
        self.td = Path(self._td.name)

    def destroy_tmp(self):
        """Clean up any garbage left in the temp directory
        """
        self._td.cleanup()

    def tearDown(self):
        """Default teardown.  Destroys tempoary directory 
        using .destory_tmp()
        """
        self.destroy_tmp()
