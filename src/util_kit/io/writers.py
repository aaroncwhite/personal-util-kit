from dask import dataframe as dd
from dask.delayed import delayed
import tempfile
import os
import time
import pandas as pd


def write_single_csv(dask_df, f, log=None, **kwargs):
    """Writes a dask dataframe to disk in one file using a context 
    manager

    Dask.DataFrame.to_csv() would normally write out each partition 
    into multiple files follow some glob pattern.  This will write out
    one file and append each partition's data onto it.  It currently
    uses a temporary directory to write out each partition's data and then
    iterates over the csvs to create one final version. 

    Ex.
    >>> x = dd.read_csv('some_csv.csv')
    >>> x.groupby('id').apply(len)
    >>> write_dask_csv(x, 'my_output_file.csv')
    
    Arguments:
        dask_graph {dask.dataframe} -- The dask dataframe with a built graph of operations
        f {file} -- The file path to write to, best if atomic
        log {logging.log object} -- Optional.  If provided, time and temp directory will be output

    Keyword Arguments:
        kwargs (dict): passed to to_csv

    Returns:
        nothing
    """
    
    start = time.time()
    # Write the parts out to a temp directory and then read each file back in and 
    # append to one final file
    with tempfile.TemporaryDirectory() as td, open(f, 'w') as file_output:
        
        if log:
            log.debug('Writing partitions to temp dir: {}'.format(td))
        
        # This will write out each partition's data to a file and insert the 
        # partition number in the * (part-1.csv, part-2.csv, etc)
        dask_df.to_csv(os.path.join(td, 'part-*.csv'), **kwargs)

        # Now read each file one by one and output the results to 
        # our final file.  This is somewhat inefficient, but dask was having trouble
        # staying in scope when using a dd.compute method...
        for f in [os.path.join(td, fn) for fn in os.listdir(td)]:
            df = pd.read_csv(f)
            df.to_csv(file_output, 
                      mode = 'a', 
                      header= file_output.tell() == 0, 
                      **kwargs)

    if log:                
        log.debug('Completed dask df to csv write in {}'.format(str(time.time() - start)))
