from unittest import TestCase
import tempfile 
from pathlib import Path
import os
import pandas as pd
import numpy as np
import dask.dataframe as dd

from .writers import *
from ..testing import TempdirTestCase


class TestSingleCSV(TempdirTestCase):
    # Testing luigi is kind of annoying
    # define a basic task here for testing
    
    def setUp(self):
        self.make_tmp()
        
        df = pd.DataFrame(np.random.randint(0,100,size=(100, 4)), columns=list('ABCD'))

        p = self.td / 'test.csv'
        self.tf_csv = p

        df.to_csv(p, index=False)

        self.df = df

    def test_single_csv_writer(self):
        ddf = dd.from_pandas(self.df, npartitions=4)
        write_single_csv(ddf, self.td / 'test_single.csv', index=False)
        ndf = pd.read_csv(self.td /'test_single.csv')
        # self.assertTrue(self.df.equals(ndf))


    