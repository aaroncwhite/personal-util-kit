from luigi import LocalTarget
from luigi.local_target import LocalFileSystem
# import dask.dataframe as dd
import pandas as pd
from glob import glob
from contextlib import contextmanager
import os
import json
import yaml

class DaskCSVTarget(LocalTarget):
    """A lightly modified LocalTarget for use with Luigi
    and CSV data in Dask
    
    Overrides the .exist() method to see if any pattern matches.
    Note, this does not check if the pattern matched is a valid
    set of csv files.  The path pattern matches what dask.dataframe.read_csv
    expects.

    Ex.:

    >>> dt = DaskCSVTarget('test/dir/pattern_*.csv')
    >>> dt.exists()
    False
   
    """

    def exists(self):
        """Return true if we find any files matching the stated pattern
        """
        f = glob(self.path)
        if f:
            return True
        else:
            return False


class DaskParquetTarget(LocalTarget):
    """A luigi parquet target to play nice with Dask
    
    Overrides the .exist() method to check for the parquet
    folder and dask's '_common_metadata" which is written last
    during a write to disk.  

    Note: this will not work on non-dask parquets!
    """


    def exists(self):
        """Return true if _common_metadata found in self.path
        """
        if os.path.exists(self.path):
            return '_common_metadata' in os.listdir(self.path)
        else:
            return False

