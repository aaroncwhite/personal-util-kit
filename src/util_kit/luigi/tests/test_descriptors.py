from unittest import TestCase
import tempfile 
from pathlib import Path
import os
import pandas as pd
import numpy as np
import dask.dataframe as dd
import json
import yaml
from hashlib import sha256
from luigi import Task, LocalTarget, Parameter

from ..target import *
from ..descriptors import *
from ...testing import TempdirTestCase



class DummyTask(Task):
    # Note: this would never typically work
    # with multiple different types of imports
    loc        = Parameter()

    csv_pandas_import   = PandasCSVImport()
    csv_dask_import     = DaskCSVImport() # note this works because no pattern in the path
    json_import         = JsonImport()
    yaml_import         = YamlImport()

    hash            = FileHash()

    def output(self):
        return LocalTarget(self.loc)


class TestImports(TempdirTestCase):
    # Testing luigi is kind of annoying
    # define a basic task here for testing
    
    def setUp(self):
        self.make_tmp()
        
        df = pd.DataFrame(np.random.randint(0,100,size=(100, 4)), columns=list('ABCD'))

        p = self.td / 'test.csv'
        self.tf_csv = p

        df.to_csv(p)

        self.tf_json = p.with_name('test.json')
        with self.tf_json.open('w') as f:
            f.write(json.dumps(df.to_dict(orient='records')))

        self.tf_yaml = p.with_name('test.yaml')
        with self.tf_yaml.open('w') as f:
            f.write(yaml.dump(df.to_dict(orient='records')))

        self.tf_known_hash = p.with_name('test_hash.txt')
        with self.tf_known_hash.open('w') as f:
            f.write('hi there')


    def test_imports(self):
        imports = [imp for imp in dir(DummyTask) if imp.endswith('import')]
        for i in imports:
            with self.subTest(i=i):
                loc_type = i.split('_')[0]
                t = DummyTask(loc=getattr(self, f'tf_{loc_type}').as_posix())
                # Call the import method
                getattr(t, i)()


    def test_hash(self):
        # FileHash can do multiple types, but we'll just test one
        t = DummyTask(loc=self.tf_known_hash.as_posix())
        h = sha256('hi there'.encode()).hexdigest()
        self.assertEqual(t.hash, h)
