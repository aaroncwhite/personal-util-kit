from unittest import TestCase
import tempfile 
from pathlib import Path
import os

from ..target import *


class TestDaskCSVTarget(TestCase):
    # Because this extends a luigi.LocalTarget
    # only the overridden method is tested

    def setUp(self):
        self._td = tempfile.TemporaryDirectory()
        self.td = Path(self._td.name)

        for i in range(0,4):
            p = self.td / f'test_{i}.csv'
            p.touch()

    def tearDown(self):
        self._td.cleanup()

    def test_exists(self):
        x = DaskCSVTarget(self.td / 'test_*.csv')
        self.assertTrue(x.exists())

    def test_not_exist(self):
        x = DaskCSVTarget('x_*.csv')
        self.assertFalse(x.exists())


class TestDaskParquetTarget(TestCase):


    def setUp(self):
        self._td = tempfile.TemporaryDirectory()
        self.td = Path(self._td.name)
        p = self.td / 'fake_parquet' / '_common_metadata'
        # make the directory first
        os.makedirs(p.parent.as_posix())
        p.touch()

    def tearDown(self):
        self._td.cleanup()

    def test_exists(self):
        x = DaskParquetTarget(self.td / 'fake_parquet')
        self.assertTrue(x.exists())

    def test_not_exist(self):
        # Because this is not recursive, test on any directory other
        # than the sample data
        x = DaskParquetTarget(self.td)
        self.assertFalse(x.exists())