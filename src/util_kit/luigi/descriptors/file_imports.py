# import dask.dataframe as dd
import pandas as pd
import dask.dataframe as dd
from glob import glob
from contextlib import contextmanager
import hashlib
import os
import json
import yaml


class BaseImport:

    def __init__(self, *args, **kwargs):
        self.kwargs = kwargs

    def __get__(self, task, cls):
        return lambda: self(task)



class PandasExcelImport(BaseImport):
    """Descriptor to add an import data function onto any task and
    return a pandas dataframe.  Make sure the output is correct!
    
    Returns:
        pandas.dataframe -- a pandas dataframe represnetation from self.output()
    """

    def __call__(self, task):
        """Utility method to get a dask data frame from an already existing source.
        **kwargs are passed to pandas.read_csv()
        """

        # Note this is using the .path attribute since we want dask to handle the globbing
        # not sure how else to handle this
        df = pd.read_excel(task.output().path, **self.kwargs)

        return df


class DaskCSVImport(BaseImport):
    """Descriptor to add an import data function onto any task and
    return a dask dataframe.  Make sure the output is correct!
    
    Returns:
        dask.dataframe -- a dask dataframe represnetation from self.output()
    """

    def __call__(self, task):
        """Utility method to get a dask data frame from an already existing source.
        **kwargs are passed to pandas.read_csv()
        """

        # Note this is using the .path attribute since we want dask to handle the globbing
        # not sure how else to handle this
        df = dd.read_csv(task.output().path, **self.kwargs)

        return df


class DaskParquetImport(BaseImport):
    """Descriptorg to add import data function on any task and 
    return a dask data frame.  Must be for a parquet data output

    Returns:
        dask.dataframe -- a dask dataframe from self.output()
    """

    def __call__(self, task):
        """Utility method to get a dask dataframe from a parquet source
        **kwargs are passed to read_parquet()
        """

        df = dd.read_parquet(task.output().path, **self.kwargs)

        return df


class PandasCSVImport(BaseImport):
    """Descriptor to add an import data function onto any task and
    return a pandas dataframe.  Make sure the output is correct!
    
    Returns:
        pd.DataFrame -- a pandas dataframe from self.output()
    """

    def __call__(self, task):
        """Utility method to get a dask data frame from an already existing source.
        **kwargs are passed to pandas.read_csv()
        """
        df = pd.read_csv(task.output().path, **self.kwargs)

        return df

class JsonImport(BaseImport):
    """Descriptor to add an import data function onto any task
    and return a parsed dict from a json file. 
    """

    def __call__(self, task):
        """Utility method to read the json file.  kwargs are passed to 
        json.load()
        """
        with task.output().open() as f:
            data = json.load(f, **self.kwargs)

        return data


class YamlImport(BaseImport):
    """Descriptor to add an import data function onto any task
    and return a parsed dict from a json file. 
    """

    def __get__(self, task, cls):
        return lambda: self(task)

    def __call__(self, task):
        """Utility method to read the json file.  kwargs are passed to 
        json.load()
        """
        with task.output().open() as f:
            data = yaml.safe_load(f, **self.kwargs)

        return data