# Note that Luigi is not considered part of the prod requirements
# in order to keep dependencies down if not using luigi

class Requirement:
    """Composition method to define requirements in a luigi task
    """
    def __init__(self, task_class, **params):
        self.task_class = task_class
        self.params = params

    def __get__(self, task, cls):
        if task:
            return task.clone(
                self.task_class,
                **self.params)
        else:
            return self


class Requires:
    """Composition to replace :meth:`luigi.task.Task.requires`
    Example::
        class MyTask(Task):
            # Replace task.requires()
            requires = Requires()  
            other = Requirement(OtherTask)
            def run(self):
                # Convenient access here...
                with self.other.output().open('r') as f:
                    ...
        >>> MyTask().requires()
        {'other': OtherTask()}
    """

    def __get__(self, task, cls):
        # Bind self/task in a closure
        return lambda: self(task)

    def __call__(self, task):
        """Returns the requirements of a task
        Assumes the task class has :class:`.Requirement` descriptors, which
        can clone the appropriate dependences from the task instance.
        Arguments:
            task {luigi.Task} -- the task to find requirements for
        Returns:
            dict: a dict of requirements 
            
        """
        # Search task.__class__ for Requirement instances
        # Should this be a recursive requirements check?
        return {k: getattr(task, k) for k in dir(task.__class__)
                if isinstance(getattr(task.__class__, k), Requirement)}


