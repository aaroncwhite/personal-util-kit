import hashlib
import inspect
import os

class FileHash:
    """Descriptor to add a hashed file output for any task's output. 
    Returns the hexdigest of the hash

    hash_algo {str} -- the hashing method to use can be any thing from 
        hashlib module. Default sha256. 
    """

    def __init__(self, hash_algo='sha256'):
        self.hash_func = hashlib.__dict__[hash_algo]

    def __get__(self, task, cls):
        if task:
            with task.output().open() as f:
                lines = f.read()

            return self.hash_func(lines.encode()).hexdigest()
        else:
            return self
