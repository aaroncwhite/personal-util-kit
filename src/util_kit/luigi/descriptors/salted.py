from luigi import LocalTarget
from luigi.task import flatten

from hashlib import sha256
import inspect
import os


def get_salted_version(task, salt_source=True, salt_params=False, hash_len=6, add_salt=None):
    """Create a salted id/version for this task and lineage
    Arguments:
        task {luigi.Task} -- a Luigi task to salt
    
    Keyword Arguments:
        salt_source {bool} -- Get hashes of the source code of the tasks
        salt_params {bool, optional} -- If task parameters should also be included. Defaults to True.
        hash_len {int, optional} -- The number of characters from the hash to include.  Defaults to 6. 
        add_salt {str} -- add any additional salt. Default: None.
    Returns:
        str: a unique, deterministic hexdigest for this task
    """
    msg = ""

    # Salt with lineage
    for req in flatten(task.requires()):
        # Note that order is important and impacts the hash - if task
        # requirements are a dict, then consider doing this is sorted order
        msg += get_salted_version(req, 
                                  salt_source=salt_source,
                                  salt_params=salt_params, 
                                  hash_len=hash_len)

    # Uniquely specify this task
    if salt_source:
        msg += ','.join([

            # Hash task class source as a way of tracking versions
            # if the source code of the task changes, then the version
            # will automatically be incremented.  Yes, if the task is only slightly
            # modified then the hash will be different, but if we are changing 
            # the source still, then that arguably should be considered a change. 
            # A stable, production impelmentation should not need to be edited further. 
            inspect.getsource(task.__class__)

        ])

    if salt_params:
        msg += ','.join([
            # Depending on strictness - skipping params is acceptable if
            # output already is partitioned by their params; including every
            # param may make hash *too* sensitive
            '{}={}'.format(param_name, repr(task.param_kwargs[param_name]))
            for param_name, param in sorted(task.get_params())
            if param.significant
        ])
    
    if add_salt:
        msg += add_salt

    return sha256(msg.encode()).hexdigest()[:hash_len]


class SaltedOutput:
    """Creates a "salted output" descriptor
    """

    def __init__(self, 
                path,
                target_type = LocalTarget,
                salt_source=True,
                salt_params=True,
                salt_length=6,
                add_salt=None,
                **kwargs):
        """Initialize a salted output
        Arguments:
            path (str): A pattern with a "{salt}" keyword defined in the string. 
                used with str.format() to create the name.  salt and task are passed as keyword args
                to format for access to any task attributes desired.
            target_type (target, optional): Defaults to LocalTarget. 
            salt_source (bool, optional): Defaults to True. If the output should actually be salted
            salt_params (bool, optional): Defaults to True. If parameters should also be included
            salt_length (int, optional): Defaults to 6. The number of characters to extract from the hex hash
            add_salt (str): Add additional str to salt the output
        Returns:
            target:  A valid luigi target 
        """
        # Check the path for a "{salt}" placeholder
        if "{salt}" not in path:
            raise ValueError("Specified path does not have a {salt} placeholder.")

        self.path = path
        self.target_type = target_type
        self.salt_source = salt_source
        self.salt_params = salt_params
        self.salt_length = salt_length
        self.add_salt    = add_salt
        self.kwargs = kwargs

    def __get__(self, task, cls):
        if task:
            return lambda: self(task)
        else:
            return self


    def __call__(self, task):
        """A target file path formed with a 'hash' kwarg.  
        """
        if task:
            if self.add_salt:
                add = self.add_salt.format(task=task)

            salt = get_salted_version(
                task = task, 
                salt_source = self.salt_source,
                salt_params=self.salt_params, 
                hash_len=self.salt_length,
                add_salt= add if add else None)

            file_path = self.path.format(salt=salt, task=task)
            return self.target_type(file_path, **self.kwargs)
        else:
            return self