from unittest import TestCase
import tempfile 
from pathlib import Path
import os
import pandas as pd
import numpy as np
import copy

from .cleaner import *
from ..testing import TempdirTestCase


class ExampleCleaner(DataCleaner):
    def clean_yes(self, col):
        """Replace 'Yes' with 1 and everything else with 0
        """
        return ((col.str.strip() == 'Yes') & (~pd.isnull(col))).astype(int)
    
     
    def clean_any_answer(self, col):
        """Mark any answer as 1
        """
        return (~pd.isnull(col)).astype(int)
 
    def clean_prompt(self, col):
        """Clean any text that starts with "Please tell ...:"
        """
        return col.str.replace(r'(^hi there:\s*)', '')

    

class TestSingleCSV(TempdirTestCase):
    # Testing luigi is kind of annoying
    # define a basic task here for testing

    df1 = pd.DataFrame({
        'messy_yes' : ['', None, 'Yes', ''],
        'anything_not_na' : [None, None, None, 'hi'],
        'messy_text' : ['hi there: this is an answer', '', '', '']
    })    

    df2 = pd.DataFrame({
        'messy_ys' : ['', None, 'Yes', ''],
        'messy_txt' : ['hi there: this is an answer', '', '', '']
    })  

    good_config = {
        'default_cleaners' : [
            'clean_any_answer'
        ],
        'columns' : {
            'text' : {
                'names' : [
                    'messy_text',
                    'messy_txt'
                ],
                'clean': True,
                'cleaners' : ['clean_prompt'],
                'required' : True
            },
            'yes' : {
                'names' : [
                    'messy_yes', 
                    'messy_ys'
                ],
                'clean' : True,
                'cleaners' : ['clean_yes'],
                'required': True
            },
            'all1' : {
                'names' : [
                    'anything_not_na', 
                    'not_na'
                ],
                'clean' : True
            }
        }
    }

    bad_config = {
        'default_cleaners' : [
            'clean_it'
        ],
        'columns' : {
            'text' : {
                'names' : [
                    'messy_text',
                    'messy_txt'
                ],
                'clean' : True,
                'required' : True
            }
        }
    }

    def test_cleaner_setup(self):
        self.assertRaisesRegex(InvalidConfigException, 'cleaning methods are incorrect', ExampleCleaner, self.bad_config)
        bc = copy.deepcopy(self.bad_config)
        bc.pop('default_cleaners')
        self.assertRaisesRegex(InvalidConfigException, 'clean=True', ExampleCleaner, bc)

        bc = copy.deepcopy(self.good_config)
        bc['columns']['text'].pop('names')
        self.assertRaisesRegex(InvalidConfigException, 'missing a required "name" key', ExampleCleaner, bc)

        self.assertTrue(callable(ExampleCleaner(self.good_config)))

    def test_cleaner_clean(self):
        cleaner = ExampleCleaner(self.good_config)

        cleaned_df1 = cleaner(self.df1)
        cleaned_df2 = cleaner(self.df2)

        self.assertEqual(cleaned_df1.text.iloc[0], 'this is an answer')
        self.assertEqual(cleaned_df2.text.iloc[0], 'this is an answer')
        self.assertEqual(cleaned_df1.all1.iloc[-1], 1)
        self.assertEqual(cleaned_df1.yes.iloc[0], 0)
