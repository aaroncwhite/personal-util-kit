import pandas as pd
import yaml

_flatten = lambda l: [item for sublist in l for item in sublist]

class InvalidConfigException(Exception):
    pass

class DataCleaner(object):
    """Cleans an input dataframe to be a useable format 
    for analysis.  This does not do any one hot encoding
    or manipulation pre-modeling.  

    When cleaning, column names are automatically stripped of
    whitespace and dropped to lowercase to attempt to match
    as many as possible. If any column is set to required=True,
    a final step of dropping missing cases for those columns is 
    applied.

    Arguments:
        config {dict} -- a dictionary mapping output column names with 
            potential names to be found and if they are required
        strict {bool} -- be strict about at least one column having a 'required' key
            in the inpug config
    Raises:
        InvalidConfigException -- if validations fail
        

    Ex.:

    >>> config = {
        "default_cleaners" : [
            "clean_any_answer"
        ],
        "columns" : {
          "text" : {
            "names" : [
                    "comment text",
                    "text"
                ]
            },
            "sentiment" : {
                "names" : [
                    "feeling",
                    "satisfaction",
                    "contentedness"
                ],
                ## This will default to "default_cleaners"
                "clean" : True 
            },
            "important" : {
                "names" : [
                    "important response value",
                    "response value"
                ],
                "clean" : True,
                "cleaners" : [
                    'custom_clean_method',
                    'custom_clean_method1'
                ]
            }
        }
      
    } 

    >>> cleaner = DataCleaner(config = config)
    >>> cleaned_df = cleaner(df)

    """

    def __init__(self, config, strict=True):
        self.strict = strict
        self._validate_config(config)
        self._parse_config(config)



    def __call__(self, df):
        """Run all cleaning methods on a dataframe 
        and return the result
        """
        out = df.copy()
        out = self._clean_header(out)
        out = self._clean_columns(out)
        out = self._clean_incomplete(out)
        
        return out

    @classmethod
    def from_yaml(cls, path, child_key=None, **kwargs):
        """Create a data frame cleaner directly from a 
        yaml file that matches the configuration format

        Arguments:
            path {str or os.Path} -- where to load the configuration file
        
        Keyword Arguments:
            child_key {str} -- If the config has more than just 
                data cleaner parameters defined, define to access a second 
                level key. Default: None.
            passed to DataCleaner()

        Returns:
            DataCleaner -- a data cleaner instance if validations pass

        """
        with open(path, 'r') as f:
            config = yaml.safe_load(f)

        if child_key:
            config = config[child_key]
        
        return cls(config=config, **kwargs)


    def _validate_config(self, config):
        """Validates a configuration file for required attributes 
        and types
        """
        issues = []
        
        # Check the names 
        check_name = [k for k,v in config['columns'].items() if not v.get('names',None) or not isinstance(v.get('names', []), list)]
        if check_name:
            base_msg = 'The following keys are missing a required "name" key and/or are not of type "list": '
            suggested_replacement = str({k : {'names' : ['name1', 'Name2', 'Name3']} for k in check_name})
            issues.append('\n{base_msg}{invalid_keys}\n\nCurrent: \n{current}\n\nSuggested replacement: \n{replacement}'.format(base_msg=base_msg,
                                                                                              current=str({k:v for k,v in config.items() if k in check_name}),
                                                                                              invalid_keys=', '.join(check_name),
                                                                                              replacement=suggested_replacement))

        check_clean = _flatten([k for k,v in config['columns'].items() if v.get('clean', None) and not v.get('cleaners', None)])
        if check_clean and not config.get('default_cleaners'):
            msg = 'clean=True for columns {} with no specific cleaners defined and no default cleaners were found!'.format(', '.join(check_clean))
            issues.append(msg)
            
        check_cleaners = _flatten([v['cleaners'] for k,v in config['columns'].items() if v.get('cleaners', None)])
        if config.get('default_cleaners', None):
            check_cleaners += config.get('default_cleaners')
            
        incorrect_cleaners = []
        for cleaner in set(check_cleaners):
            try:
                callable(getattr(self, cleaner))
            except:
                incorrect_cleaners.append(cleaner)

        if incorrect_cleaners:
            base_msg = 'The following cleaning methods are incorrect or not callable:\n{}'.format(', '.join(incorrect_cleaners))
            issues.append(base_msg)
            
        if issues:
            raise InvalidConfigException('\n'+'\n'.join(issues))
    
    
    def _parse_config(self, config):
        required = [k for k,v in config['columns'].items() if v.get('required', None)]
        
        if self.strict and not required:
            example = str({'col1': {'name' : ['Human Column Name'], 'required': True}})
            raise InvalidConfigException('\nNo required column definitions found.  Set strict=False to ignore or define a required column.\n\nExample:\n{example}'.format(example=example))

        for k,v in config['columns'].items():
            if v.get('clean', None) and not v.get('cleaners',None):
                v['cleaners'] = config['default_cleaners']
        
        expected = _flatten([v['names'] for v in config['columns'].values()])

        
        
        # Parse the columns we need to rename and make a mapping for pandas later
        unique_renames = set([(vi, k) for k,v in config['columns'].items() for vi in v['names']])
        self.renames = {v[0].strip().lower():v[1] for v in unique_renames}
        self.required = required
        self.expected = expected
        self.config = config

    def _clean_header(self, df):
        """Identifies the proper header column based on the 
        config defined.  First attempts to find any of the names
        defined and then checks for `required` keys that must exist
        """
        out = df.copy()
        out = self._fix_header(out)
        out = self._rename_header(out)
        return out

    def _fix_header(self, df):
        i = 0
        # This is redundant with the loop...
        headers = df.columns

        any_check = headers.isin(self.expected).any()

        while not any_check:
            headers = df.iloc[i,:]
            any_check = headers.isin(self.expected).any()
            if any_check:
                break
            i += 1

        df.columns = [str(c).strip().lower() for c in headers]
        if i > 0:
            df = df.iloc[(i+1):,:]

        return df
    
    def _rename_header(self, df):
        return df.rename(self.renames, axis=1)
    
    def _clean_columns(self, df):
        """Clean any columns that have clean=True in the config
        """
        df = df.loc[:,df.columns.isin(self.config['columns'].keys())].copy()
        for col, cleaners in {k:v['cleaners'] for k,v in self.config['columns'].items() if v.get('clean', None)}.items():
            if col in df.columns:
                for cleaner in cleaners:
                    print(col, cleaner)
                    df[col] = getattr(self, cleaner)(df[col])
            
        return df
    
    def _clean_incomplete(self, df):
        if self.required:
            df = df.dropna(subset=self.required)
        return df