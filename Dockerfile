FROM python:3.7 AS base
ARG DEV
ARG CI_USER_TOKEN
RUN echo -e "machine gitlab.com\nlogin gitlab-ci-token\npassword ${CI_USER_TOKEN}" > ~/.netrc


ENV \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    PIPENV_HIDE_EMOJIS=true \
    PIPENV_COLORBLIND=true \
    PIPENV_NOSPIN=true \
    PYTHONPATH="/app:${PYTHONPATH}"

# pip installing pipenv right now is causing errors due to some
# package incorrectly requiring functools32.  
# appears related to https://github.com/pypa/pipenv/issues/515
# install direct from source
RUN apt-get install -y git && \
    pip install git+https://github.com/pypa/pipenv#egg=pipenv

# Copy the app
# then rely on a volume mount over /app
# with current code as developing
WORKDIR /app
COPY . .
RUN pipenv install --system --deploy --ignore-pipfile --dev
