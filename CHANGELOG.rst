
Changelog
=========

0.5.5
------------------

* Clean up jupyter garbage from repo

0.5.4
------------------

* Fix signature of `WeightedLoss` call method to accept `sample_weight`

0.5.3
------------------

* pypi pipenv is causing issues when locking.  Use bleeding edge from github instead. 
* upgrade build environment to 3.7
* add tests and CI


0.5.2
------------------

* Fix args stored as kwargs for HyperparameterConfig
* TODO: actually write tests

0.5.1
------------------

* Fix bug with hyperopt name stored as kwarg

0.5.0
------------------

* Convert HyperparameterConfig to use `hyperopt`

0.4.2
------------------

* Remove set_model_methods from BaseModel

0.4.1
------------------

* Tensorflow GPU only (?)

0.4.0
------------------

* Convert Luigi import mixins to descriptors

0.3.1
------------------

* Fix hashed_file only returning function not hash

0.3.0
------------------
* Add hashed_file mixin for Luigi tasks


0.2.0
------------------

* Bugfix missing dependencies
* Add salted outputs

0.1.1 
------------------

* Fix malformed setup.py
* Update for docker config with gitlab

0.1.0 (2019-03-23)
------------------

* Pull in utilities from various projects
