util-kit
========

.. testsetup::

    from util-kit import *

.. automodule:: util-kit
    :members:
